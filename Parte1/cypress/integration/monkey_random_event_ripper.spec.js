describe('Los estudiantes under monkeys', function() {
	it('visits los estudiantes and survives monkeys', function() {
		cy.visit('https://losestudiantes.co');
		cy.contains('Cerrar').click();
		cy.wait(1000);
		randomEvent(10);
	})
})

function randomLink() {
	cy.get('a').then($links => {
		var randomLink = $links.get(getRandomInt(0, $links.length));
		if(!Cypress.dom.isHidden(randomLink)) {
			cy.wrap(randomLink).click({force: true});
		}
	});
}

function randomText() {
	cy.get('input').then($inputs => {
		var randomText = $inputs.get(getRandomInt(0, $inputs.length));
		if(!Cypress.dom.isHidden(randomText)) {
			cy.wrap(randomText).type('Este es un texto de prueba');
		}
	});
}

function randomCombo() {
	cy.get('select').then($combos => {
		var randomCombo = $combos.get(getRandomInt(0, $combos.length));
		if(!Cypress.dom.isHidden(randomCombo)) {
			var element = cy.wrap(randomCombo)			
			element.then($options => {
                            element.select($options.get(0).value);
                        });
		}
	});
}

function randomButton() {
	cy.get('button').then($buttons => {
		var randomButton = $buttons.get(getRandomInt(0, $buttons.length));
		if(!Cypress.dom.isHidden(randomButton)) {
			cy.wrap(randomButton).click();
		}
	});
}

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
}

function randomEvent(monkeysLeft) {
	var monkeysLeft = monkeysLeft;
	if (monkeysLeft > 0) {
		var event = getRandomInt(0, 4);
		switch (event) {
			case 0:
				randomLink();
				break;
			case 1:
				randomText();
				break;
			case 2:
				randomCombo();
				break;
			case 3:
				randomButton();
				break;
		}
		monkeysLeft = monkeysLeft - 1;
		setTimeout(randomEvent(monkeysLeft), 1000);
	}
}
